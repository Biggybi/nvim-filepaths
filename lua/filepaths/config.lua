local M = {}

local defaults = {
  max_width = 80,
  min_width = 20,
  hint_width = 4, -- width of the first column
  right_padding = 4,
  ---@type 'name' | 'mini' | 'icon' | 'type'
  left_column = "icon", -- what to display in the left column
  overflow_padding = 4,
  lost_width = 6, -- size of borders, paddings, etc, inherent to telescope
}

M.options = defaults

function M.set(user_options) M.options = vim.tbl_extend("force", defaults, user_options or {}) end

return M

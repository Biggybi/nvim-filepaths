local M = {}

function M.get_git_file_path_url()
  local remote_url = vim.system({ "git", "remote", "get-url", "origin" }):wait()
  if remote_url.code ~= 0 then return nil end
  print("DEBUGPRINT[3]: utils.lua:13: remote_url=" .. vim.inspect(remote_url))
  local current_branch_name = vim.system({ "git", "branch", "--show-current" }):wait()
  local path = vim.fn.expand("%")
  return remote_url.stdout:gsub("\n", ""):gsub("%.git", "")
    .. "/blob/"
    .. current_branch_name.stdout:gsub("\n", "")
    .. "/"
    .. path
end

function M.is_real_file() return vim.fn.filereadable(vim.fn.expand("%")) == 1 end

return M

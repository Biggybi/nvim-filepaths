local M = {}

local actions = require("telescope.actions")
local actions_state = require("telescope.actions.state")
local entry_display = require("telescope.pickers.entry_display")
local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local config = require("telescope.config").values

local options = require("filepaths.config").options

local log = require("plenary.log"):new()
log.level = "debug"

---@class Path
---@field type string
---@field mini string
---@field icon string
---@field name string|nil

---@return Path[]
function M.file_paths()
  ---@type Path[]
  local paths = {
    {
      type = "name",
      mini = "%",
      icon = "",
      name = vim.fn.expand("%:t"),
    },
    {
      type = "path",
      mini = ".",
      icon = "",
      name = vim.fn.expand("%"),
    },
    {
      type = "home",
      mini = "~",
      icon = "󰜥",
      name = vim.fn.expand("%:~"),
    },
    {
      type = "root",
      mini = "/",
      icon = "",
      name = vim.fn.expand("%:p:h"),
    },
    {
      type = "git",
      mini = "@",
      icon = "",
      name = require("filepaths.utils").get_git_file_path_url(),
    },
  }
  return vim.tbl_filter(function(v) return v.name ~= nil end, paths)
end

local displayer = entry_display.create({
  separator = " ",
  items = {
    { width = options.hint_width },
    { remaining = true },
  },
})

M.yank_system = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  vim.notify("Yanked (system): " .. selection.value.name, 2)
  vim.fn.setreg("+", selection.value.name)
end

M.yank_unnamed = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  vim.notify("Yanked: " .. selection.value.name, 2)
  vim.fn.setreg('"', selection.value.name)
end

M.paste_after_cursor = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  vim.api.nvim_paste(selection.value.name, true, -1)
end

M.paste_before_cursor = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  if vim.fn.col(".") ~= 1 then vim.cmd("normal! h") end
  vim.api.nvim_paste(selection.value.name, true, -1)
end

M.paste_above_cursor = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  vim.api.nvim_put({ selection.value.name }, "l", false, false)
end

M.paste_below_cursor = function(prompt_bufnr)
  local selection = actions_state.get_selected_entry()
  actions.close(prompt_bufnr)
  vim.api.nvim_put({ selection.value.name }, "l", true, false)
end

M.list_paths = function(opts)
  if require("filepaths.utils").is_real_file() == false then
    vim.notify("No file name", 2)
    return
  end

  pickers
    .new(opts, {
      finder = finders.new_table({
        results = M.file_paths(),
        entry_maker = function(entry)
          if entry.name == nil then return end
          return {
            value = entry,
            display = function()
              return displayer({
                { entry[options.left_column], "TelescopeResultsNumber" },
                entry.name,
              })
            end,
            ordinal = entry.name .. ":" .. entry.type,
          }
        end,
      }),
      layout_strategy = "cursor",
      layout_config = {
        height = function(self) return #self.finder.results + 4 end,
        width = function(res)
          local width = 0
          for _, v in ipairs(res.finder.results) do
            if #v.value.name > width then width = #v.value.name end
          end
          width = width + options.hint_width + options.lost_width + options.right_padding
          if options.min_width > width then return options.min_width end
          if options.max_width < width then return options.max_width end
          return width
        end,
      },
      sorter = config.generic_sorter(opts),
      prompt_title = "File Paths",

      attach_mappings = function(prompt_bufnr, map)
        actions.select_default:replace(function() M.yank_unnamed(prompt_bufnr) end)
        map("i", "y", function() M.yank_unnamed(prompt_bufnr) end)
        map("i", "Y", function() M.yank_system(prompt_bufnr) end)
        map("i", "<leader>y", function() M.yank_system(prompt_bufnr) end)
        map("i", "<c-a>", function() M.paste_after_cursor(prompt_bufnr) end)
        map("i", "<c-p>", function() M.paste_before_cursor(prompt_bufnr) end)
        map("i", "<c-i>", function() M.paste_before_cursor(prompt_bufnr) end)
        map("i", "P", function() M.paste_above_cursor(prompt_bufnr) end)
        map("i", "p", function() M.paste_below_cursor(prompt_bufnr) end)
        return true
      end,
    })
    :find()
end

return M
